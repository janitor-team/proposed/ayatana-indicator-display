Source: ayatana-indicator-display
Section: misc
Priority: optional
Maintainer: Ayatana Packagers <pkg-ayatana-devel@lists.alioth.debian.org>
Uploaders:
 Mike Gabriel <sunweaver@debian.org>,
 Debian UBports Team <team+ubports@tracker.debian.org>,
Build-Depends: cmake,
               cmake-extras (>= 0.10),
               dbus,
               intltool,
               libayatana-common-dev (>= 0.9.3),
               libglib2.0-dev (>= 2.36),
               libgtest-dev,
               libgudev-1.0-dev,
               libproperties-cpp-dev,
# for coverage reports
               gcovr,
               lcov,
# for tests
               qtbase5-dev,
               libqtdbusmock1-dev,
               libqtdbustest1-dev,
               cppcheck,
               googletest,
# for packaging
               debhelper-compat (= 13),
# for systemd unit
               systemd [linux-any],
Standards-Version: 4.6.1
Rules-Requires-Root: no
Homepage: https://github.com/AyatanaIndicators/ayatana-indicator-display
Vcs-Git: https://salsa.debian.org/debian-ayatana-team/ayatana-indicator-display.git
Vcs-Browser: https://salsa.debian.org/debian-ayatana-team/ayatana-indicator-display/

Package: ayatana-indicator-display
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ayatana-indicator-common,
Description: Ayatana Indicator for Display configuration
 This Ayatana Indicator is designed to be placed on the right side of a
 panel and give the user easy control for changing their display settings.
 .
 Ayatana Indicators are only available on desktop environments that
 provide a renderer for system indicators (such as MATE, Xfce, Lomiri,
 etc.).
